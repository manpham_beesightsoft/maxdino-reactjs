import React from 'react';
import HomePage from './Containers/HomePage';
import './styles.scss';

class App extends React.Component {
    render() {
        return( 
        <> 
            <HomePage />
        </>);
    }
}
export default App;