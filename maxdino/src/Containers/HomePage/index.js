import React from 'react';
import Header from '../../components/Header';
import NewsList from '../../components/NewsList';
import VideoList from '../../components/VideoList';
import Post from '../../components/Post';
import Feed from '../../components/Feed';
import ConnectionList from '../../components/ConnectionList';
import './styles.scss';

class HomePage extends React.Component {
    render() {
        return( 
        <> 
            <Header />
            <NewsList  />
            <div className="container"> 
                <div className="row">
                    <div className="col-sm-3">
                        <VideoList />
                    </div>
                    <div className="col-sm-6">
                        <Post />
                        <Feed />
                    </div>
                    <div className="col-sm-3">
                        <ConnectionList />
                    </div>
                </div>
            </div>
        </>);
    }
}
export default HomePage;