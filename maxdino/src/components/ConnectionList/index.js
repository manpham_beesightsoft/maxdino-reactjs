import React from 'react';
import Connection from '../Connection'
import './styles.scss';
class ConnectionList extends React.Component {

    render() {
        return (
            <div className="section section-connection">
                <div className="section-title">
                    <b>CONNECTION ACTIVITIES</b>
                </div>
           
                <div className="list-item-connection">
                    <Connection />
                    <Connection />
                    <Connection />
                    <Connection />
                </div>
            </div>

        );
    }
}
export default ConnectionList;