import React from 'react';
import './styles.scss';
import avatarMale from '../../assets/images/avatar-male.png';
import feed from '../../assets/images/img-news.png';
class Feed extends React.Component {
    render() {
        return ( <div className="section section-post feed">
        <div className="item-feed">
          <div className="dropdown">
            <a href="/"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style={{position: 'absolute', right: 0}}><i className="fa fa-ellipsis-h" /> <span className="caret" /></a>
            <ul className="dropdown-menu">
              <li><a href="/">Action</a></li>
              <li><a href="/">Another action</a></li>
            </ul>
          </div>
          <div className="head-post">
            <div className="avatar-user"><img src={avatarMale} alt="avatar" /></div>
            <div className="head-post-content">
              <div className="head-post-name-user">David Lord</div>
              <div className="head-post-story-subtitle">
                <div className="date"><span className="icon-calendar" />Today, 11 Dec, 2017</div>
                <div className="public">
                  <span className="icon-public" />
                  <div className="dropdown">
                    <a href="/" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Everyone <span className="caret" /></a>
                    <ul className="dropdown-menu">
                      <li><a href="/">Action</a></li>
                      <li><a href="/">Another action</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="content-post">
            <p className="feed-text">So… That’s it <a href="/">#china</a></p>
            <div className="post-feed">
              <div className="post-feed-img">
                <img src={feed} alt="feed" />
              </div>
              <div className="post-feed-content">
                <div className="post-feed-title">Why are China instant noodle sales going off the boil?</div>
                <div className="post-feed-detail">Be it a snack for students, a meal on the train, or just the go-to choice for hungry workers, more than 46.2 billion packets were sold in China and Hong Kong in 2013.
                </div>
                <div className="post-feed-detail--link"><a href="/">http://www.bbc.com/news/business-42390058</a></div>
              </div>
            </div>
          </div>
          <div className="footer-post">
            <div className="ratting"><span className="icon-like" /><b>74</b> Reactions</div>
            <div className="ratting"><span className="icon-comment" /><b>0</b> Comments</div>
          </div>
        </div>
      </div>
      );
    }
}
export default Feed;