import React from 'react';
import SearchInput from '../SearchInput';
import Selection from '../Selection';
import './styles.scss';
import logo from '../../assets/images/logo.png';
import avatar from "../../assets/images/avatar-male.png";
class Header extends React.Component {
    render() {
        return (
                <nav className="navbar navbar-expand-sm navbar-maxdino">
        <a className="navbar-brand" href="/"><img src={logo} alt="logo" /></a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            {/* <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Dropdown
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="#">Action</a>
		          <a class="dropdown-item" href="#">Another action</a>
		          <div class="dropdown-divider"></div>
		          <a class="dropdown-item" href="#">Something else here</a>
		        </div>
          </li> */}
              <Selection />
        
              <SearchInput/>
              {/* <input className="form-control mr-sm-2 nav-form-input" type="search" placeholder="Ask something about Finance" aria-label="Search" />
              <div className="search"><img src={search} alt="search"/> </div> */}
         
          </ul>
          <ul className="navbar-nav nav-right">
            <li className="nav-item nav">
              <a href="/"><span className="icon-noti" /></a>
            </li>
            <li className="nav-item dropdown">
              <a href="/" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span className="caret" /><img src={avatar} alt="avatar"/></a>
              <ul className="dropdown-menu">
                <li><a href="/">Action</a></li>
                <li><a href="/">Another action</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
            
        )
    }
}

export default Header;