import React from 'react';
import './styles.scss';
import Video from '../Video';

class VideoList extends React.Component {

    render() {
        return (
            <div className="section section-video">
                <div className="section-title">
                    <b>Videos</b>
                    <a href="/">See More</a>
                </div>
                <div className="list-item-video">
                    <Video />
                    <Video />
                    <Video />
                </div>
            </div>
        );
    }
}
export default VideoList;