import React from 'react';
import './styles.scss';

class News extends React.Component {

    render() {
        const { slide }  = this.props;
        return (
                  <div className="item-news">
                    <div className="item-news-img">
                      <img src={slide} alt="news"/>  {/* day la comment*/}
                    </div>
                    <div className="item-news-content">
                      <div className="item-news-first-child-content">
                        <div className="item-news-title">Post Malone</div>
                        <div className="item-news-detail">It is a long established fact that a reader will be distracted
                          by the readable content…</div>
                      </div>
                      <div className="item-news-name">David Lord - Finacial Expert</div>
                    </div>
                  </div>
        );
    }
}
export default News;