import React from 'react';
import './styles.scss';
import videothumb from '../../assets/images/video-thumb.jpg';
class Video extends React.Component {
    render() {
        return (
            <div className="item-video">
              <div className="item-video-img">
                <img src={videothumb} alt="video" />
                <a href="/"><span className="icon-icon" /></a>
              </div>
              <div className="item-video-title">Apple Economy</div>
            </div>
        );
    }
}
export default Video;