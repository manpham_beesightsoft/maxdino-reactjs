import React from 'react';
import search from "../../assets/images/search-icon.png";
import './styles.scss';

class SearchInput extends React.Component {
    render() {
        return (
            <form className="form-inline my-2 my-lg-0 form-search">
                <input className="form-control mr-sm-2 nav-form-input" type="search" placeholder="Ask something about Finance" aria-label="Search" />
                <div className="search"><img src={search} alt="search"/> </div>
            </form>
        );
    }
}
export default SearchInput;