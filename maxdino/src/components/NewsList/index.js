import React from 'react';
import News from '../News';
import './styles.scss';
import  slide1 from '../../assets/images/slide-item-1.png';
import slide2 from '../../assets/images/slide-item-2.png';
import slide3 from '../../assets/images/slide-item-3.png';

class NewsList extends React.Component {

    render() {

        return(
            <div className="section section-news p-0">
              <div className="container">
                <div className="list-item-news">
                  <div className="owl-carousel owl-theme">
                        <News slide={slide1}/> 
                        <News slide={slide2}/> 
                        <News slide={slide3}/>   
                        <News slide={slide1}/>       
                  </div>
                </div>
              </div>
            </div>
        );
    }
}
export default NewsList;