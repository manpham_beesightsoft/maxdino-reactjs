import React from 'react';
import avatarMale from '../../assets/images/avatar-male.png';
import './styles.scss';
class Connection extends React.Component {

    render() {
        return(
            <div className="item-connection">
            <div className="item-connection-img">
              <img src={avatarMale} alt="avatar" />
            </div>
            <div className="item-connection-content">
              <b className="user-name">David Lord</b>
              <span>liked post</span>
              <span className="text">“EU tells Netanyahu no support for Trump’s Jerusalem move”</span>
              <span>4h ago</span>
            </div>
          </div>
        );
    }
}
export default Connection;