import React from 'react';
import './styles.scss';
import avatarUser from '../../assets/images/avatar-male.png';

class Post extends React.Component {

    render() {
        return (
            <div className="section section-post">
            <div className="head-post">
              <div className="avatar-user"><img src={avatarUser} alt="avatar" /></div>
              <div className="head-post-content">
                <div className="head-post-name-user">David Lord</div>
                <div className="head-post-story-subtitle">
                  <div className="date"><span className="icon-calendar" />Today, 11 Dec, 2017</div>
                  <div className="public">
                    <span className="icon-public" />
                    <div className="dropdown">
                      <a href="/" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Everyone <span className="caret" /></a>
                      <ul className="dropdown-menu">
                        <li><a href="/">Action</a></li>
                        <li><a href="/">Another action</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-post">
              <div className="form-group">
                <textarea className="form-control" rows={3} placeholder="Start posting today ..." defaultValue={""} />
              </div>
            </div>
            <div className="footer-post">
              <div className="row">
                <div className="col-6">
                  <a href="/"><span className="icon-camera" /></a>
                  <a href="/"><span className="icon-link" /></a>
                  <a href="/"><span className="icon-add" /></a>
                </div>
                <div className="col-6">
                  <div className="text-right">
                    Also post on
                    <span className="icon-icon-facebook" />
                    <span className="icon-icon-twitter" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
}
export default Post;