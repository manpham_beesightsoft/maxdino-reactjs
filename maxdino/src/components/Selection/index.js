import React from 'react';
import question from "../../assets/images/question-mark-icon.png";
import './styles.scss';
class Selection extends React.Component {
    render() {
        return(
        <form className="form-inline my-2 my-lg-0 form-select">
            <select className="form-control" id="inputGroupSelect01">
              <option defaultValue="Ask" className="select-ask">Ask</option>
              <option value={1}>Action</option>
              <option value={2}>Another action</option>
            </select>
            <div className="img-question-mark">
              <img src={question} alt="question mark" />
            </div>
        </form>
        );
    }
}
export default Selection;