// $('.owl-carousel').owlCarousel({
//     loop:false,
//     margin:30,
//     nav: true,
//     dots: false,
//     responsive:{
//         2000:{
//             items:3
//         }
//     }
// })

$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        loop: false,
        margin: 30,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items:1,
                nav: false,
            },
            576: {
                items:2
            },
            992: {
                items:3
            }
        }
    })
})